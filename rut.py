#-*- coding: utf-8 -*-

import sys
import re
import math
import logging


SERIE = [2, 3, 4, 5, 6, 7]


def valida_rut(rut):
    digitos, d_verificador = split_rut(rut)
    dv = get_dv(digitos)

    return True if dv == d_verificador else False


def split_rut(rut):
    return rut.replace(".", "").lower().split("-")


def get_dv(digitos):
    suma = 0
    i = 0

    for d in digitos[::-1]:
        suma += int(d) * SERIE[i]
        i = 0 if i == len(SERIE) - 1 else i + 1

    dv = 11 - (suma - 11 * int(math.floor(suma / 11.0)))

    if dv == 10:
        return "k"
    elif dv == 11:
        return "0"
    else:
        return str(dv)


def main():
    try:
        rut = sys.argv[1]

        if re.match(r"^\d{1,2}(\.|)\d{3}(\.|)\d{3}[-][\dkK]$", rut):
            res = valida_rut(rut)
            print("rut válido" if res else "rut no válido")
            sys.exit(0)
        else:
            print("Error: Formato rut no válido")

    except IndexError as e:
        print("Error: No se ingresó rut ( {} )".format(e))

    sys.exit(1)


if __name__ == "__main__":
    main()
