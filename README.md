# py-rut-validator

Validador de rut

## Uso

```
python rut.py <rut>
```

## Input
Se acepta `rut` en formato con y sin puntos, y debe tener guión

Ej.
- `17641380-8` válido
- `17.641.380-8` válido
- `176413808` no válido

## Test

Con pytest

```
pytest
```