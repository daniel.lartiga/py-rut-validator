import rut as r
import sys
import pytest


@pytest.mark.parametrize("rut, res", [
    ("17641380-8", True),
    ("17641380-9", False),
    ("17641384-0", True),
    ("17641393-k", True),
    ("17641393-K", True),
])
def test_valida_rut(rut, res):
    assert r.valida_rut(rut) == res


@pytest.mark.parametrize("sys_argv, exit_code", [
    (["rut.py", "17.641.380-8"], 0),    # formato valido con puntos
    (["rut.py", "17641380-8"], 0),      # formato valido sin puntos
    (["rut.py", "17641393-k"], 0),      # formato valido con k
    (["rut.py", "176413808"], 1),       # formato invalido, sin guion 
    (["rut.py", "172.641.380-8"], 1),   # formato invalido digitos extra
    (["rut.py", "gatos"], 1),           # gatos
    (["rut.py"], 1),                    # sin input
])
def test_main(sys_argv, exit_code):
    sys.argv = sys_argv
    with pytest.raises(SystemExit) as sys_exit:
        r.main()
    assert sys_exit.type == SystemExit
    assert sys_exit.value.code == exit_code


@pytest.mark.parametrize("digitos, dv", [
    ("17641380", "8"),
    ("17641384", "0"),
    ("17641393", "k"),
])
def test_get_dv(digitos, dv):
    assert r.get_dv(digitos) == dv


def test_split_rut_k():
    assert r.split_rut("17.641.393-K") == ["17641393", "k"]
